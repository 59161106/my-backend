const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  gender: {
    required: true,
    type: String,
    enum: ['M', 'F']
  }
})

module.exports = mongoose.model('User', userSchema)
